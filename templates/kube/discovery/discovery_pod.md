Discovery pod
--------------------------------------------------------------------------------

## Description
Creates a zabbix host for each pod discovered in the kubernetes cluster.

Zabbix pod hosts are created with the "Template Kube Pod by Prom API" template by default.

The pod name `{#PODNAME}` is used as a hostname to allow further discovery in the 
pod template.

## Data sample
[discovery_pod.json](../data/discovery_pod.json) is the data file generated with
the following request:
```
http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=kube_pod_created
```

## Discovery rule
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Discovery pod                                          |
| Type                | HTTP Agent                                             |
| Key                 | `prom.pod.discovery`                                   |
| URL                 | `{$PROM.API.URL}/query`                                |

| Query Fields        | Value                                                  |
|---------------------|--------------------------------------------------------|
| query               | `kube_pod_created`                                     |

## Preprocessing
| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| JSONPath            | `$.data.result[?(@.metric.namespace=~'{$PROM.POD.NAMESPACE.MATCHES}' && @.metric.service=~'{$PROM.POD.SERVICE.MATCHES}' && @.metric.pod=~'{$PROM.POD.NAME.MATCHES}')].metric` |

## LLD macros
| LLD macro           | JSONPath                                               |
|---------------------|--------------------------------------------------------|
| `{#NAMESPACE}`      | `$.namespace`                                          |
| `{#SERVICE}`        | `$.service`                                            |
| `{#PODNAME}`        | `$.pod`                                                |

## Filters
| Filter macro        | Condition(And) | Regular expression                    |
|---------------------|----------------|---------------------------------------|
| `{#NAMESPACE}`      | does not match | `{$PROM.POD.NAMESPACE.NOT_MATCHES}`   |
| `{#SERVICE}`        | does not match | `{$PROM.POD.SERVICE.NOT_MATCHES}`     |
| `{#PODNAME}`        | does not match | `{$PROM.POD.NAME.NOT_MATCHES}`        |

## Host prototype
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Host name           | `{#PODNAME}`                                           |
| Visible name        |                                                        |
| Groups              | Kubernetes/Pods                                        |
| Templates           | Template Kube Pod by Prom API                          |
| Inventory           | Disabled                                               |
