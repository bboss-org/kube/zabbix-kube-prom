Discovery node
--------------------------------------------------------------------------------

## Description
Creates a zabbix host for each node discovered in the kubernetes cluster.

Zabbix hosts are created with the "Template Kube Node by Prom API" template by
default.

## Data sample
[discovery_node.json](../data/discovery_node.json) is the data file generated with
the following request:
```
http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=kubelet_node_name
```

## Discovery rule
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Discovery node                                         |
| Type                | HTTP Agent                                             |
| Key                 | `prom.node.discovery`                                  |
| URL                 | `{$PROM.API.URL}/query`                                |

| Query Fields        | Value                                                  |
|---------------------|--------------------------------------------------------|
| query               | `kubelet_node_name`                                    |

## Preprocessing
| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| JSONPath            | `$.data.result[?(@.metric.node=~'{$PROM.NODE.NAME.MATCHES}')].metric` |
| JavaScript          | (See content below)                                    |

```javascript
return JSON.stringify(JSON.parse(value).map(function(metric){
  metric.instance=metric.instance.split(":")[0]; return metric}))
```

## LLD macros
| LLD macro           | JSONPath                                               |
|---------------------|--------------------------------------------------------|
| `{#NODE.IP}`        | `$.instance`                                           |
| `{#NODE.NAME}`      | `$.node`                                               |

## Filters
| Filter macro        | Condition(And) | Regular expression                    |
|---------------------|----------------|---------------------------------------|
| `{#NODE.IP}`        | matches        | `{$PROM.NODE.IP.MATCHES}`             |
| `{#NODE.IP}`        | does not match | `{$PROM.NODE.IP.NOT_MATCHES}`         |
| `{#NODE.NAME}`      | does not match | `{$PROM.NODE.NAME.NOT_MATCHES}`       |

## Host prototype
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Host name           | `{#NODE.IP}`                                           |
| Visible name        | `{#NODE.NAME}`                                         |
| Groups              | Kubernetes/Nodes                                       |
| Templates           | Template Kube Node by Prom API                         |
| Inventory           | Disabled                                               |
