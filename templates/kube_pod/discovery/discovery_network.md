Discovery network
-----------------------------------------------------------

## Description
Creates an item for each metric per network interface with "Metrics network" datasource.

## Discovery rule
| Field       | Value                                            |
|-------------|--------------------------------------------------|
| Name        | Discovery network                                |
| Type        | Dependent item                                   |
| Key         | `prom.pod.discovery[network]`                    |
| Master item | `Template Kube Pod by Prom API: Metrics network` |

| LLD macro    | JSONPath               |
|--------------|------------------------|
| `{#IFNAME}`  | `$.metric.interface`   |
| `{#METRIC}`  | `$.metric['__name__']` |

## Item prototypes
| Field               | Value                                            |
|---------------------|--------------------------------------------------|
| Name                | `Network {#IFNAME}: {#METRIC}`                   |
| Type                | Dependent item                                   |
| Key                 | `prom.pod.metrics[network,{#METRIC},{#IFNAME}]`  |
| Master item         | `Template Kube Pod by Prom API: Metrics network` |
| Type of information | `Numeric (float)`                                |
| Applications        | `Network {#IFNAME}`                              |

| Preprocessing       | Parameters                                                                            |
|---------------------|---------------------------------------------------------------------------------------|
| JSONPath            | `$[?(@.metric['__name__']=='{#METRIC}' && @.metric.interface=='{#IFNAME}')].value[1]` |
| JavaScript          | `return JSON.parse(value).map(Number);`                                               |
