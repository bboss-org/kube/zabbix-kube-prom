Discovery monitoring
-----------------------------------------------------------

## Description
Creates an item for each metric per type and result with "Metrics monitoring" datasource.

## Discovery rule
| Field       | Value                                               |
|-------------|-----------------------------------------------------|
| Name        | Discovery monitoring                                |
| Type        | Dependent item                                      |
| Key         | `prom.pod.discovery[monitoring]`                    |
| Master item | `Template Kube Pod by Prom API: Metrics monitoring` |

| LLD macro   | JSONPath               |
|-------------|------------------------|
| `{#METRIC}` | `$.metric['__name__']` |
| `{#RESULT}` | `$.metric.result`      |
| `{#TYPE}`   | `$.metric.probe_type`  |

## Item prototypes
| Field               | Value                                                      |
|---------------------|------------------------------------------------------------|
| Name                | `{#METRIC} ({#TYPE},{#RESULT})`                            |
| Type                | Dependent item                                             |
| Key                 | `prom.pod.metrics[monitoring,{#METRIC},{#TYPE},{#RESULT}]` |
| Master item         | `Template Kube Pod by Prom API: Metrics monitoring`        |
| Type of information | `Numeric (float)`                                          |
| Applications        | Monitoring                                                 |

| Preprocessing       | Parameters                                                                                                           |
|---------------------|----------------------------------------------------------------------------------------------------------------------|
| JSONPath            | `$[?(@.metric['__name__']=='{#METRIC}' && @.metric.probe_type=='{#TYPE}' && @.metric.result=='{#RESULT}')].value[1]` |
| JavaScript          | `return JSON.parse(value).map(Number);`                                                                              |
