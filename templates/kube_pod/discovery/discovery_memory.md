Discovery memory
-----------------------------------------------------------

## Description
Creates an item for each metric per container with "Metrics memory" datasource.

## Discovery rule
| Field       | Value                                           |
|-------------|-------------------------------------------------|
| Name        | Discovery memory                                |
| Type        | Dependent item                                  |
| Key         | `prom.pod.discovery[memory]`                    |
| Master item | `Template Kube Pod by Prom API: Metrics memory` |

| LLD macro      | JSONPath               |
|----------------|------------------------|
| `{#CONTAINER}` | `$.metric.container`   |
| `{#METRIC}`    | `$.metric['__name__']` |

## Item prototypes
| Field               | Value                                             |
|---------------------|---------------------------------------------------|
| Name                | `{#CONTAINER} - {#METRIC}`                        |
| Type                | Dependent item                                    |
| Key                 | `prom.pod.metrics[memory,{#CONTAINER},{#METRIC}]` |
| Master item         | `Template Kube Pod by Prom API: Metrics memory`   |
| Type of information | `Numeric (float)`                                 |
| Applications        | Memory                                            |

| Preprocessing       | Parameters                                                                               |
|---------------------|------------------------------------------------------------------------------------------|
| JSONPath            | `$[?(@.metric['__name__']=='{#METRIC}' && @.metric.container=='{#CONTAINER}')].value[1]` |
| JavaScript          | `return JSON.parse(value).map(Number);`                                                  |
