Discovery storage
-----------------------------------------------------------

## Description
Creates an item for each metric per container with "Metrics storage" datasource.

## Discovery rule
| Field       | Value                                            |
|-------------|--------------------------------------------------|
| Name        | Discovery storage                                |
| Type        | Dependent item                                   |
| Key         | `prom.pod.discovery[storage]`                    |
| Master item | `Template Kube Pod by Prom API: Metrics storage` |

| LLD macro      | JSONPath               |
|----------------|------------------------|
| `{#CONTAINER}` | `$.metric.container`   |
| `{#METRIC}`    | `$.metric['__name__']` |

## Item prototypes
| Field               | Value                                              |
|---------------------|----------------------------------------------------|
| Name                | `{#CONTAINER} - {#METRIC}`                         |
| Type                | Dependent item                                     |
| Key                 | `prom.pod.metrics[storage,{#CONTAINER},{#METRIC}]` |
| Master item         | `Template Kube Pod by Prom API: Metrics storage`   |
| Type of information | `Numeric (float)`                                  |
| Applications        | Storage                                            |

| Preprocessing       | Parameters                                                                               |
|---------------------|------------------------------------------------------------------------------------------|
| JSONPath            | `$[?(@.metric['__name__']=='{#METRIC}' && @.metric.container=='{#CONTAINER}')].value[1]` |
| JavaScript          | `return JSON.parse(value).map(Number);`                                                  |
