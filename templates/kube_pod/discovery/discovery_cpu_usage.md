Discovery cpu_usage
-----------------------------------------------------------

## Description
Creates an item for each metric per container with "Metrics cpu_usage" datasource.

## Discovery rule
| Field       | Value                                              |
|-------------|----------------------------------------------------|
| Name        | Discovery cpu_usage                                |
| Type        | Dependent item                                     |
| Key         | `prom.pod.discovery[cpu_usage]`                    |
| Master item | `Template Kube Pod by Prom API: Metrics cpu_usage` |

| LLD macro      | JSONPath               |
|----------------|------------------------|
| `{#CONTAINER}` | `$.metric.container`   |
| `{#METRIC}`    | `$.metric['__name__']` |

## Item prototypes
| Field               | Value                                                                        |
|---------------------|------------------------------------------------------------------------------|
| Name                | `{#CONTAINER} - container_cpu_usage_seconds_total`                           |
| Type                | Dependent item                                                               |
| Key                 | `prom.pod.metrics[cpu_usage,{#CONTAINER},container_cpu_usage_seconds_total]` |
| Master item         | `Template Kube Pod by Prom API: Metrics cpu_usage`                           |
| Type of information | `Numeric (float)`                                                            |
| Applications        | CPU                                                                          |

| Preprocessing       | Parameters                                                                               |
|---------------------|------------------------------------------------------------------------------------------|
| JSONPath            | `$[?(@.metric['__name__']=='{#METRIC}' && @.metric.container=='{#CONTAINER}')].value[1]` |
| JavaScript          | `return JSON.parse(value).map(Number);`                                                  |

## Graph prototypes
| Field | Value                                                     |
|-------|-----------------------------------------------------------|
| Name  | `{#CONTAINER} - container_cpu_usage_seconds_total`        |
| Items | `Template Kube Pod by Prom API: {#CONTAINER} - {#METRIC}` |
