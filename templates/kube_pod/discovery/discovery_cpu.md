Discovery cpu
-----------------------------------------------------------

## Description
Creates an item for each metric per container with "Metrics cpu" datasource.

## Discovery rule
| Field       | Value                                        |
|-------------|----------------------------------------------|
| Name        | Discovery cpu                                |
| Type        | Dependent item                               |
| Key         | `prom.pod.discovery[cpu]`                    |
| Master item | `Template Kube Pod by Prom API: Metrics cpu` |

| LLD macro      | JSONPath               |
|----------------|------------------------|
| `{#CONTAINER}` | `$.metric.container`   |
| `{#METRIC}`    | `$.metric['__name__']` |

## Item prototypes
| Field               | Value                                          |
|---------------------|------------------------------------------------|
| Name                | `{#CONTAINER} - {#METRIC}`                     |
| Type                | Dependent item                                 |
| Key                 | `prom.pod.metrics[cpu,{#CONTAINER},{#METRIC}]` |
| Master item         | `Template Kube Pod by Prom API: Metrics cpu`   |
| Type of information | `Numeric (float)`                              |
| Applications        | CPU                                            |

| Preprocessing       | Parameters                                                                               |
|---------------------|------------------------------------------------------------------------------------------|
| JSONPath            | `$[?(@.metric['__name__']=='{#METRIC}' && @.metric.container=='{#CONTAINER}')].value[1]` |
| JavaScript          | `return JSON.parse(value).map(Number);`                                                  |

## Graph prototypes
| Field | Value                                                     |
|-------|-----------------------------------------------------------|
| Name  | `{#CONTAINER} - {#METRIC}`                                |
| Items | `Template Kube Pod by Prom API: {#CONTAINER} - {#METRIC}` |
