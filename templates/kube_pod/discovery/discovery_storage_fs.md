Discovery storage_fs
-----------------------------------------------------------

## Description
Creates an item for each metric per container and storage device with "Metrics storage_fs" datasource.

## Discovery rule
| Field       | Value                                               |
|-------------|-----------------------------------------------------|
| Name        | Discovery storage_fs                                |
| Type        | Dependent item                                      |
| Key         | `prom.pod.discovery[storage,fs]`                    |
| Master item | `Template Kube Pod by Prom API: Metrics storage_fs` |

| LLD macro      | JSONPath               |
|----------------|------------------------|
| `{#CONTAINER}` | `$.metric.container`   |
| `{#DEVICE}`    | `$.metric.device`      |
| `{#METRIC}`    | `$.metric['__name__']` |

## Item prototypes
| Field               | Value                                                        |
|---------------------|--------------------------------------------------------------|
| Name                | `{#CONTAINER} - Storage {#DEVICE}: {#METRIC}`                |
| Type                | Dependent item                                               |
| Key                 | `prom.pod.metrics[storage,{#CONTAINER},{#METRIC},{#DEVICE}]` |
| Master item         | `Template Kube Pod by Prom API: Metrics storage_fs`          |
| Type of information | `Numeric (float)`                                            |
| Applications        | `Storage {#DEVICE}`                                          |

| Preprocessing       | Parameters                                                                                                               |
|---------------------|--------------------------------------------------------------------------------------------------------------------------|
| JSONPath            | `$[?(@.metric['__name__']=='{#METRIC}' && @.metric.container=='{#CONTAINER}' && @.metric.device=='{#DEVICE}')].value[1]` |
| JavaScript          | `return JSON.parse(value).map(Number);`                                                                                  |
