Discovery spec
-----------------------------------------------------------

## Description
Creates an item for each metric per container with "Metrics spec" datasource.

## Discovery rule
| Field       | Value                                         |
|-------------|-----------------------------------------------|
| Name        | Discovery spec                                |
| Type        | Dependent item                                |
| Key         | `prom.pod.discovery[spec]`                    |
| Master item | `Template Kube Pod by Prom API: Metrics spec` |

| LLD macro      | JSONPath               |
|----------------|------------------------|
| `{#CONTAINER}` | `$.metric.container`   |
| `{#METRIC}`    | `$.metric['__name__']` |

## Item prototypes
| Field               | Value                                           |
|---------------------|-------------------------------------------------|
| Name                | `{#CONTAINER} - {#METRIC}`                      |
| Type                | Dependent item                                  |
| Key                 | `prom.pod.metrics[spec,{#CONTAINER},{#METRIC}]` |
| Master item         | `Template Kube Pod by Prom API: Metrics spec`   |
| Type of information | `Numeric (float)`                               |
| Applications        | Spec                                            |

| Preprocessing       | Parameters                                                                               |
|---------------------|------------------------------------------------------------------------------------------|
| JSONPath            | `$[?(@.metric['__name__']=='{#METRIC}' && @.metric.container=='{#CONTAINER}')].value[1]` |
| JavaScript          | `return JSON.parse(value).map(Number);`                                                  |
