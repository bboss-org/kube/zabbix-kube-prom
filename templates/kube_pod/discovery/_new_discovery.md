_New Discovery
-----------------------------------------------------------

## Description
Creates an item for each metric per container with "_New Metrics" datasource.

The aim of _New discovery is to show metrics not already managed by all other discoveries.

Disabled by default, enable it to find specific application metrics.

## Discovery rule
| Field       | Value                                         |
|-------------|-----------------------------------------------|
| Name        | _New Discovery                                |
| Type        | Dependent item                                |
| Key         | `prom.pod.discovery[new]`                     |
| Master item | `Template Kube Pod by Prom API: _New Metrics` |

| LLD macro      | JSONPath               |
|----------------|------------------------|
| `{#CONTAINER}` | `$.metric.container`   |
| `{#METRIC}`    | `$.metric['__name__']` |

| Filter macro | Condition(And) | Regular expression                    |
|--------------|----------------|---------------------------------------|
| `{#METRIC}`  | does not match | `^container_cpu_.*$`                  |
| `{#METRIC}`  | does not match | `^container_memory_.*$`               |
| `{#METRIC}`  | does not match | `^prober_.*$`                         |
| `{#METRIC}`  | does not match | `^container_spec_.*$`                 |
| `{#METRIC}`  | does not match | `^.*container_(file|ulimits|log)_.*$` |
| `{#METRIC}`  | does not match | `^container_fs_.*$`                   |
| `{#METRIC}`  | does not match | `^kube_pod_.*$`                       |

## Item prototypes
| Field               | Value                                          |
|---------------------|------------------------------------------------|
| Name                | `{#CONTAINER} - {#METRIC}`                     |
| Type                | Dependent item                                 |
| Key                 | `prom.pod.metrics[new,{#CONTAINER},{#METRIC}]` |
| Master item         | `Template Kube Pod by Prom API: _New Metrics`  |
| Type of information | `Numeric (float)`                              |
| Applications        | _New Metrics                                   |

| Preprocessing       | Parameters                                                                               |
|---------------------|------------------------------------------------------------------------------------------|
| JSONPath            | `$[?(@.metric['__name__']=='{#METRIC}' && @.metric.container=='{#CONTAINER}')].value[1]` |
| JavaScript          | `return JSON.parse(value).map(Number);`                                                  |
