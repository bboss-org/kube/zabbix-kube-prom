Metrics spec
-----------------------------------------------------------

## Description
Retrieve all existing spec metrics for the specified pod (Zabbix host)

## Data sample
[metrics_spec.json](../data/metrics_spec.json) is the data result file (`$.data.result`) for the following request:
```
http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^container_spec_.*$",pod="v12-9-2-grafana-764845d886-fkzgg",container!="POD",container!=""}) by (__name__,container)
```

## Item
| Field               | Value                    |
|---------------------|--------------------------|
| Name                | Metrics spec             |
| Type                | HTTP Agent               |
| Key                 | `prom.pod.metrics[spec]` |
| URL                 | `{$PROM.API.URL}/query`  |
| Type of information | `Text`                   |
| Applications        | _Raw items               |

| Query Fields | Value                                                                                                             |
|--------------|-------------------------------------------------------------------------------------------------------------------|
| query        | `sum({__name__=~"^container_spec_.*$",pod="{HOST.NAME}",container!="POD",container!=""}) by (__name__,container)` |

| Preprocessing | Parameters      |
|---------------|-----------------|
| JSONPath      | `$.data.result` |
