Metrics storage_fs
-----------------------------------------------------------

## Description
Retrieve all existing filesystem metrics for the specified pod (Zabbix host)

## Data sample
[metrics_storage_fs.json](../data/metrics_storage_fs.json) is the data result file (`$.data.result`) for the following request:
```
http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^container_fs_.*$",pod="v12-9-2-grafana-764845d886-fkzgg",container!="POD",container!=""}) by (__name__,container,device)
```

## Item
| Field               | Value                          |
|---------------------|--------------------------------|
| Name                | Metrics storage_fs             |
| Type                | HTTP Agent                     |
| Key                 | `prom.pod.metrics[storage,fs]` |
| URL                 | `{$PROM.API.URL}/query`        |
| Type of information | `Text`                         |
| Applications        | _Raw items                     |

| Query Fields | Value                                                                                                                  |
|--------------|------------------------------------------------------------------------------------------------------------------------|
| query        | `sum({__name__=~"^container_fs_.*$",pod="{HOST.NAME}",container!="POD",container!=""}) by (__name__,container,device)` |

| Preprocessing | Parameters      |
|---------------|-----------------|
| JSONPath      | `$.data.result` |
