Metrics memory
-----------------------------------------------------------

## Description
Retrieve all existing memory metrics for the specified pod (Zabbix host)

## Data sample
[metrics_memory.json](../data/metrics_memory.json) is the data result file (`$.data.result`) for the following request:
```
http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^container_memory_.*$",pod="v12-9-2-grafana-764845d886-xxg2m",container!="POD",container!=""}) by (__name__,container)
```

## Item
| Field               | Value                      |
|---------------------|----------------------------|
| Name                | Metrics memory             |
| Type                | HTTP Agent                 |
| Key                 | `prom.pod.metrics[memory]` |
| URL                 | `{$PROM.API.URL}/query`    |
| Type of information | `Text`                     |
| Applications        | _Raw items                 |

| Query Fields | Value                                                                                                               |
|--------------|---------------------------------------------------------------------------------------------------------------------|
| query        | `sum({__name__=~"^container_memory_.*$",pod="{HOST.NAME}",container!="POD",container!=""}) by (__name__,container)` |

| Preprocessing | Parameters      |
|---------------|-----------------|
| JSONPath      | `$.data.result` |
