Metrics cpu_usage
-----------------------------------------------------------

## Description
CPU usage per containers for the specified pod (Zabbix host)

## Data sample
[metrics_cpu_usage.json](../data/metrics_cpu_usage.json) is the data result file (`$.data.result`) for the following request:
```
http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__="node_namespace_pod_container:container_cpu_usage_seconds_total:sum_rate",pod="v12-9-2-grafana-764845d886-xxg2m", container!="POD"}) by (__name__,container)
```

## Item
| Field               | Value                         |
|---------------------|-------------------------------|
| Name                | Metrics cpu_usage             |
| Type                | HTTP Agent                    |
| Key                 | `prom.pod.metrics[cpu_usage]` |
| URL                 | `{$PROM.API.URL}/query`       |
| Type of information | `Text`                        |
| Applications        | _Raw items                    |

| Query Fields | Value                                                                                                                                                  |
|--------------|--------------------------------------------------------------------------------------------------------------------------------------------------------|
| query        | `sum({__name__="node_namespace_pod_container:container_cpu_usage_seconds_total:sum_rate",pod="{HOST.NAME}",container!="POD"}) by (__name__,container)` |

## Preprocessing
| Preprocessing | Parameters      |
|---------------|-----------------|
| JSONPath      | `$.data.result` |
