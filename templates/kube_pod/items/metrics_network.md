Metrics network
-----------------------------------------------------------

## Description
Retrieve all existing network metrics for the specified pod (Zabbix host)

## Data sample
[metrics_network.json](../data/metrics_network.json) is the data result file (`$.data.result`) for the following request:
```
http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^container_network_.*$",pod="v12-9-2-grafana-764845d886-fkzgg",container="POD"}) by (__name__,interface)
```

## Item
| Field               | Value                       |
|---------------------|-----------------------------|
| Name                | Metrics network             |
| Type                | HTTP Agent                  |
| Key                 | `prom.pod.metrics[network]` |
| URL                 | `{$PROM.API.URL}/query`     |
| Type of information | `Text`                      |
| Applications        | _Raw items                  |

| Query Fields | Value                                                                                                 |
|--------------|-------------------------------------------------------------------------------------------------------|
| query        | `sum({__name__=~"^container_network_.*$",pod="{HOST.NAME}",container="POD"}) by (__name__,interface)` |

| Preprocessing | Parameters      |
|---------------|-----------------|
| JSONPath      | `$.data.result` |
