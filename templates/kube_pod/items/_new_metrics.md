_New Metrics
-----------------------------------------------------------

## Description
Retrieve all existing metrics for the specified pod (Zabbix host)

The aim of _New Metrics is to get all existing metrics to identify, with _New Discovery, metrics not already managed by all other discoveries.

Disabled by default, enable it to find specific new application metrics and create new items and discoveries.

## Data sample
[_new_metrics.json](../data/_new_metrics.json) is the data result file (`$.data.result`) for the following request:
```
http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query={pod="v12-9-2-grafana-764845d886-fkzgg",container!="POD",container!=""}
```

## Item
| Field               | Value                   |
|---------------------|-------------------------|
| Name                | _New Metrics            |
| Type                | HTTP Agent              |
| Key                 | `prom.pod.metrics[new]` |
| URL                 | `{$PROM.API.URL}/query` |
| Type of information | `Text`                  |
| Applications        | _Raw items              |

| Query Fields | Value                                                |
|--------------|------------------------------------------------------|
| query        | `{pod="{HOST.NAME}",container!="POD",container!=""}` |

## Preprocessing
| Preprocessing | Parameters      |
|---------------|-----------------|
| JSONPath      | `$.data.result` |
