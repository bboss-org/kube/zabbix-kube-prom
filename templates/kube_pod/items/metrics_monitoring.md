Metrics monitoring
-----------------------------------------------------------

## Description
Retrieve all existing monitoring metrics for the specified pod (Zabbix host)

## Data sample
[metrics_monitoring.json](../data/metrics_monitoring.json) is the data result file (`$.data.result`) for the following request:
```
http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^prober_.*$",pod="v12-9-2-grafana-764845d886-fkzgg",container!="POD",container!=""}) by(__name__,container,probe_type,result)
```

## Item
| Field               | Value                          |
|---------------------|--------------------------------|
| Name                | Metrics monitoring             |
| Type                | HTTP Agent                     |
| Key                 | `prom.pod.metrics[monitoring]` |
| URL                 | `{$PROM.API.URL}/query`        |
| Type of information | `Text`                         |
| Applications        | _Raw items                     |

| Query Fields | Value                                                                                                                      |
|--------------|----------------------------------------------------------------------------------------------------------------------------|
| query        | `sum({__name__=~"^prober_.*$",pod="{HOST.NAME}",container!="POD",container!=""}) by(__name__,container,probe_type,result)` |

| Preprocessing | Parameters      |
|---------------|-----------------|
| JSONPath      | `$.data.result` |
