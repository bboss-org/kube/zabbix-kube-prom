Data sample
--------------------------------------------------------------------------------

Files inside the data directory contain sample data used to test Preprocesing stages.

Raw JSON answer have been stripped of the request status to ensure file content is the expected result only (`$.data.result`).

Following table describes which HTTP request provides the corresponding file content.

| File                                                       | HTTP Request    |
|------------------------------------------------------------|-----------------|
| [metrics_all.json](metrics_all.json)                       | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query={__name__=~"^node_.*$",instance="172.16.50.10:9100",container="node-exporter"}` |
| [metrics_all_sum.json](metrics_all_sum.json)               | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^node_.*$",instance="172.16.50.10:9100",container="node-exporter"}) by (__name__,cpu,mode,device,ifalias,operstate,filesystem,mountpoint,fstype,nodename,machine,sysname,release,version)` |
| [metrics_cpu.json](metrics_cpu.json)                       | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query={__name__=~"^node_(?:cpu|load|intr|context).*$",instance="172.16.50.10:9100",container="node-exporter"}` |
| [metrics_cpu_sum.json](metrics_cpu_sum.json)               | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^node_(?:cpu|load|intr|context).*$",instance="172.16.50.10:9100",container="node-exporter"}) by (__name__,cpu,mode)` |
| [metrics_disk.json](metrics_disk.json)                     | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query={__name__=~"^node_disk.*$",instance="172.16.50.10:9100",container="node-exporter"}` |
| [metrics_disk_sum.json](metrics_disk_sum.json)             | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^node_disk.*$",instance="172.16.50.10:9100",container="node-exporter"}) by (__name__,device)` |
| [metrics_filesystem.json](metrics_filesystem.json)         | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query={__name__=~"^node_filesystem.*$",instance="172.16.50.10:9100",container="node-exporter"}` |
| [metrics_filesystem_sum.json](metrics_filesystem_sum.json) | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^node_filesystem.*$",instance="172.16.50.10:9100",container="node-exporter"}) by (__name__,device,mountpoint,fstype)` |
| [metrics_general.json](metrics_general.json)               | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query={__name__=~"^node_(?:filefd|boot|uname|time_).*$",instance="172.16.50.10:9100",container="node-exporter"}` |
| [metrics_general_sum.json](metrics_general_sum.json)       | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^node_(?:filefd|boot|uname|time_).*$",instance="172.16.50.10:9100",container="node-exporter"}) by (__name__,nodename,sysname,release,version)` |
| [metrics_inventory.json](metrics_inventory.json)           | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query={__name__=~"^node_uname.*$",instance="172.16.50.10:9100",container="node-exporter"}` |
| [metrics_inventory_sum.json](metrics_inventory_sum.json)   | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^node_uname.*$",instance="172.16.50.10:9100",container="node-exporter"}) by (__name__,machine)` |
| [metrics_memory.json](metrics_memory.json)                 | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query={__name__=~"^node_(?:memory_Mem|memory_Swap).*$",instance="172.16.50.10:9100",container="node-exporter"}` |
| [metrics_memory_sum.json](metrics_memory_sum.json)         | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^node_(?:memory_Mem|memory_Swap).*$",instance="172.16.50.10:9100",container="node-exporter"}) by (__name__)` |
| [metrics_monitoring.json](metrics_monitoring.json)         | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query={__name__=~"^node_exporter.*$",instance="172.16.50.10:9100",container="node-exporter"}` |
| [metrics_monitoring_sum.json](metrics_monitoring_sum.json) | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^node_exporter.*$",instance="172.16.50.10:9100",container="node-exporter"}) by (__name__,version)` |
| [metrics_network.json](metrics_network.json)               | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query={__name__=~"^node_network.*$",instance="172.16.50.10:9100",container="node-exporter"}` |
| [metrics_network_sum.json](metrics_network_sum.json)       | `http://prometheus.k8scluster.nuci7.lan:8080/api/v1/query?query=sum({__name__=~"^node_network.*$",instance="172.16.50.10:9100",container="node-exporter"}) by (__name__,device,ifalias,operstate)` |
