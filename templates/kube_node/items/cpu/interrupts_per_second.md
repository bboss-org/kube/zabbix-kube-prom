Interrupts per second
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Interrupts per second                                  |
| Type                | Dependent item                                         |
| Key                 | `system.cpu.intr[node_exporter]`                       |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               |                                                        |
| Applications        | CPU                                                    |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `{__name__=~"node_intr"}`                              |
| Change per second   |                                                        |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Interrupts per second                                  |
| Type                |   | Dependent item                                         |
| Key                 |   | `system.cpu.intr[node_exporter]`                       |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   |                                                        |
| Applications        |   | CPU                                                    |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_intr_total')].value[1]` |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
| Change per second   |   |                                                        |
