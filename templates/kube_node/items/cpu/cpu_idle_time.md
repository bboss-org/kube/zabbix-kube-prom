CPU idle time
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | CPU idle time                                          |
| Type                | Dependent item                                         |
| Key                 | `system.cpu.idle[node_exporter]`                       |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               | %                                                      |
| Applications        | CPU                                                    |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus to JSON  | `{__name__=~"^node_cpu(?:_seconds_total)?$",cpu=~".+",mode="idle"}` |
| JavaScript          | (See content below)                                    |
| Change per second   |                                                        |
| Custom multiplier   | 100                                                    |

```javascript
//calculates average, all cpu utilization
var valueArr = JSON.parse(value);
return valueArr.reduce(function(acc,obj){
   return acc + parseFloat(obj['value'])
},0)/valueArr.length;
```

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | CPU idle time                                          |
| Type                |   | Dependent item                                         |
| Key                 |   | `system.cpu.idle[node_exporter]`                       |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   | %                                                      |
| Applications        |   | CPU                                                    |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_cpu_seconds_total' && @.metric.mode=='idle')].value[1]` |
| JavaScript          | * | (See content below)                                    |
| Change per second   |   |                                                        |
| Custom multiplier   |   | 100                                                    |

```javascript
//calculates average, all cpu utilization
var valueArr = JSON.parse(value);
return valueArr.reduce(function(acc,obj){
   return acc + parseFloat(obj)
},0)/valueArr.length;
```
