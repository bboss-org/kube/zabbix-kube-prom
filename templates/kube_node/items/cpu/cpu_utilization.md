CPU utilization
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | CPU utilization                                        |
| Type                | Dependent item                                         |
| Key                 | `system.cpu.util[node_exporter]`                       |
| Master item         | `Template OS Linux by Prom: CPU idle time`             |
| Type of information | `Numeric (float)`                                      |
| Units               | %                                                      |
| Applications        | CPU                                                    |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| JavaScript          | (See content below)                                    |

```javascript
//Calculate utilization
return (100 - value)
```

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | CPU utilization                                        |
| Type                |   | Dependent item                                         |
| Key                 |   | `system.cpu.util[node_exporter]`                       |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   | %                                                      |
| Applications        |   | CPU                                                    |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JavaScript          |   | (See content below)                                    |

```javascript
//Calculate utilization
return (100 - value)
```
