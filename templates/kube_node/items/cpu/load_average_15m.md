Load average (15m avg)
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Load average (15m avg)                                 |
| Type                | Dependent item                                         |
| Key                 | `system.cpu.load.avg15[node_exporter]`                 |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               |                                                        |
| Applications        | CPU                                                    |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `node_load15`                                          |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Load average (15m avg)                                 |
| Type                |   | Dependent item                                         |
| Key                 |   | `system.cpu.load.avg15[node_exporter]`                 |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   |                                                        |
| Applications        |   | CPU                                                    |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_load15')].value[1]`   |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
