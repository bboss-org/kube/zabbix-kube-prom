Number of CPUs
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Number of CPUs                                         |
| Type                | Dependent item                                         |
| Key                 | `system.cpu.num[node_exporter]`                        |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               |                                                        |
| Applications        | CPU                                                    |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus to JSON  | `{__name__=~"^node_cpu(?:_seconds_total)?$",cpu=~".+",mode="idle"}` |
| JavaScript          | (See content below)                                    |

```javascript
//count the number of cores
return JSON.parse(value).length
```

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Number of CPUs                                         |
| Type                |   | Dependent item                                         |
| Key                 |   | `system.cpu.num[node_exporter]`                        |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   | %                                                      |
| Applications        |   | CPU                                                    |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_cpu_seconds_total' && @.metric['mode']=='idle')].value[1]` |
| JavaScript          |   | (See content below)                                    |

```javascript
//count the number of cores
return JSON.parse(value).length
```
