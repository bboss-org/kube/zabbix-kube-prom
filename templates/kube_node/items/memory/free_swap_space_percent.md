Free swap space in %
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Free swap space                                        |
| Type                | Calculated                                             |
| Key                 | `system.swap.pfree[node_exporter]`                     |
| Formula             | (See content below)                                    |
| Type of information | `Numeric (float)`                                      |
| Units               | %                                                      |
| Applications        | Memory                                                 |

```javascript
last("system.swap.free[node_exporter]")/last("system.swap.total[node_exporter]")*100
```
