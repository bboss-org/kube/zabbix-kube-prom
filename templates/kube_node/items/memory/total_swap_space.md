Total swap space
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Total swap space                                       |
| Type                | Dependent item                                         |
| Key                 | `system.swap.total[node_exporter]`                     |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               | B                                                      |
| Applications        | Memory                                                 |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `{__name__=~"node_memory_SwapTotal"}`                  |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Total swap space                                       |
| Type                |   | Dependent item                                         |
| Key                 |   | `system.swap.total[node_exporter]`                     |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   | B                                                      |
| Applications        |   | Memory                                                 |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_memory_SwapTotal_bytes')].value[1]` |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
