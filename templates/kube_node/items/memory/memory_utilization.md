Memory utilization
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Memory utilization                                     |
| Type                | Calculated                                             |
| Key                 | `vm.memory.util[node_exporter]`                        |
| Formula             | (See content below)                                    |
| Type of information | `Numeric (float)`                                      |
| Units               | %                                                      |
| Applications        | Memory                                                 |

```javascript
(last("vm.memory.total[node_exporter]")-last("vm.memory.available[node_exporter]"))/last("vm.memory.total[node_exporter]")*100
```
