System uptime
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | System uptime                                          |
| Type                | Dependent item                                         |
| Key                 | `system.uptime[node_exporter]`                         |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               | uptime                                                 |
| Applications        | Status                                                 |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `{__name__=~"^node_boot_time(?:_seconds)?$"}`          |
| JavaScript          | (See content below)                                    |

```javascript
//use boottime to calculate uptime
return (Math.floor(Date.now()/1000)-Number(value));
```

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | System uptime                                          |
| Type                |   | Dependent item                                         |
| Key                 |   | `system.uptime[node_exporter]`                         |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   | uptime                                                 |
| Applications        |   | Status                                                 |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_boot_time_seconds')].value[1]` |
| JavaScript          | * | (See content below)                                    |

```javascript
//use boottime to calculate uptime
return (Math.floor(Date.now()/1000)-Number(JSON.parse(value)[0]));
```
