Version of node_exporter running
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Version of node_exporter running                       |
| Type                | Dependent item                                         |
| Key                 | `agent.version[node_exporter]`                         |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Character`                                            |
| Applications        | Monitoring agent                                       |

| Preprocessing                    | Parameters                                |
|----------------------------------|-------------------------------------------|
| Prometheus Pattern               | `node_exporter_build_info` `version`      |
| Discard unchanged with heartbeat | 1d                                        |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Version of node_exporter running                       |
| Type                |   | Dependent item                                         |
| Key                 |   | `agent.version[node_exporter]`                         |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Character`                                            |
| Applications        |   | Monitoring agent                                       |

| Preprocessing                    | M | Parameters                                |
|----------------------------------|---|-------------------------------------------|
| JSONPath                         | * | `$[?(@.metric['__name__']=='node_uname_info')].metric.version` |
| JavaScript                       | * | `return JSON.parse(value)[0];`            |
| Discard unchanged with heartbeat |   | 1d                                        |
