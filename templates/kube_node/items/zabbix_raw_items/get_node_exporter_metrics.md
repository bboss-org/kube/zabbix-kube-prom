Get node_exporter metrics
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Get node_exporter metrics                              |
| Type                | HTTP Agent                                             |
| Key                 | `node_exporter.get`                                    |
| URL                 | `http://{HOST.CONN}:{$NODE_EXPORTER_PORT}/metrics`     |
| Type of information | `Text`                                                 |
| Applications        | Zabbix raw items                                       |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Get node_exporter metrics                              |
| Type                |   | HTTP Agent                                             |
| Key                 |   | `node_exporter.get`                                    |
| URL                 | * | `{$PROM.API.URL}/query`                                |
| Type of information |   | `Text`                                                 |
| Applications        |   | Zabbix raw items                                       |

| Query Fields        | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| query               | * | `sum({__name__=~'^node_.*$',instance=~'^{HOST.HOST}:{$NODE_EXPORTER_PORT}$',container='node-exporter'}) by (__name__,cpu,mode,device,ifalias,operstate,filesystem,mountpoint,fstype,nodename,machine,sysname,release,version)` |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$.data.result`                                        |
