Number of open file descriptors
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Number of open file descriptors                        |
| Type                | Dependent item                                         |
| Key                 | `fd.open[node_exporter]`                               |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               |                                                        |
| Applications        | General                                                |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `node_filefd_allocated`                                |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Number of open file descriptors                        |
| Type                |   | Dependent item                                         |
| Key                 |   | `fd.open[node_exporter]`                               |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   |                                                        |
| Applications        |   | General                                                |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_filefd_allocated')].value[1]` |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
