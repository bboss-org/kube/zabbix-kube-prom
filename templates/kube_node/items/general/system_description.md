System description
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | System description                                     |
| Type                | Dependent item                                         |
| Key                 | `system.descr[node_exporter]`                          |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Character`                                            |
| Applications        | General                                                |

| Preprocessing                    | Parameters                                |
|----------------------------------|-------------------------------------------|
| Prometheus to JSON               | `node_uname_info`                         |
| JavaScript                       | (See content below)                       |
| Discard unchanged with heartbeat | 1d                                        |

```javascript
var info = JSON.parse(value)[0];
   return info.labels.sysname+' version: '+info.labels.release+' '+info.labels.version
```

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | System description                                     |
| Type                |   | Dependent item                                         |
| Key                 |   | `system.descr[node_exporter]`                          |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Character`                                            |
| Applications        |   | General                                                |

| Preprocessing                    | M | Parameters                                |
|----------------------------------|---|-------------------------------------------|
| JSONPath                         | * | `$[?(@.metric['__name__']=='node_uname_info')].metric` |
| JavaScript                       | * | (See content below)                       |
| Discard unchanged with heartbeat |   | 1d                                        |

```javascript
var info = JSON.parse(value)[0];
return info.sysname + ' version: ' + info.release + ' ' + info.version;
```
