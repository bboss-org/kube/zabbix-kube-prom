System name
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | System name                                            |
| Type                | Dependent item                                         |
| Key                 | `system.name[node_exporter]`                           |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Character`                                            |
| Applications        | General                                                |

| Preprocessing                    | Parameters                                |
|----------------------------------|-------------------------------------------|
| Prometheus Pattern               | `node_uname_info` `nodename`              |
| Discard unchanged with heartbeat | 1d                                        |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | System name                                            |
| Type                |   | Dependent item                                         |
| Key                 |   | `system.name[node_exporter]`                           |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Character`                                            |
| Applications        |   | General                                                |

| Preprocessing                    | M | Parameters                                |
|----------------------------------|---|-------------------------------------------|
| JSONPath                         | * | `$[?(@.metric['__name__']=='node_uname_info')].metric.nodename` |
| JavaScript                       | * | `return JSON.parse(value)[0];`            |
| Discard unchanged with heartbeat |   | 1d                                        |
