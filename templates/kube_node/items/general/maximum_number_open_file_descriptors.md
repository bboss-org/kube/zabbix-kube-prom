Maximum number of open file descriptors
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Maximum number of open file descriptors                |
| Type                | Dependent item                                         |
| Key                 | `kernel.maxfiles[node_exporter]`                       |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               |                                                        |
| Applications        | General                                                |

| Preprocessing                    | Parameters                                |
|----------------------------------|-------------------------------------------|
| Prometheus Pattern               | `node_filefd_maximum`                     |
| Discard unchanged with heartbeat | 1d                                        |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Maximum number of open file descriptors                |
| Type                |   | Dependent item                                         |
| Key                 |   | `kernel.maxfiles[node_exporter]`                       |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   |                                                        |
| Applications        |   | General                                                |

| Preprocessing                    | M | Parameters                                |
|----------------------------------|---|-------------------------------------------|
| JSONPath                         | * | `$[?(@.metric['__name__']=='node_filefd_maximum')].value[1]` |
| JavaScript                       | * | `return JSON.parse(value).map(Number)`    |
| Discard unchanged with heartbeat |   | 1d                                        |
