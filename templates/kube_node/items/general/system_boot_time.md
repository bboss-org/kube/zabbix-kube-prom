System boot time
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | System boot time                                       |
| Type                | Dependent item                                         |
| Key                 | `system.boottime[node_exporter]`                       |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               | unixtime                                               |
| Applications        | General                                                |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `{__name__=~"^node_boot_time(?:_seconds)?$"}`          |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | System boot time                                       |
| Type                |   | Dependent item                                         |
| Key                 |   | `system.boottime[node_exporter]`                       |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   | unixtime                                               |
| Applications        |   | General                                                |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_boot_time_seconds')].value[1]` |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
