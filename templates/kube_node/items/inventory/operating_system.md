Operating system
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Operating system                                       |
| Type                | Dependent item                                         |
| Key                 | `system.sw.os[node_exporter]`                          |
| Master item         | `Template OS Linux by Prom: System description`        |
| Type of information | `Character`                                            |
| Applications        | Inventory                                              |

| Preprocessing                    | Parameters                                |
|----------------------------------|-------------------------------------------|
| Discard unchanged with heartbeat | 1d                                        |
