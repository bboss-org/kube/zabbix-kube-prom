Operating system architecture
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Operating system architecture                          |
| Type                | Dependent item                                         |
| Key                 | `system.sw.arch[node_exporter]`                        |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Character`                                            |
| Applications        | Inventory                                              |

| Preprocessing                    | Parameters                                |
|----------------------------------|-------------------------------------------|
| Prometheus Pattern               | `node_uname_info` `machine`               |
| Discard unchanged with heartbeat | 1d                                        |
    
## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Operating system architecture                          |
| Type                |   | Dependent item                                         |
| Key                 |   | `system.sw.arch[node_exporter]`                        |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Character`                                            |
| Applications        |   | Inventory                                              |

| Preprocessing                    | M | Parameters                                |
|----------------------------------|---|-------------------------------------------|
| JSONPath                         | * | `$[?(@.metric['__name__']=='node_uname_info')].metric.machine` |
| JavaScript                       | * | `return JSON.parse(value)[0];`            |
| Discard unchanged with heartbeat |   | 1d                                        |
