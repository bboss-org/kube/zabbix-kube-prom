Network interface discovery
--------------------------------------------------------------------------------

## Original Rule (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Network interface discovery                            |
| Type                | Dependent item                                         |
| Key                 | `net.if.discovery[node_exporter]`                      |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus to JSON  | `{__name__=~"^node_network_info$"}`                    |

| LLD macro           | JSONPath                                               |
|---------------------|--------------------------------------------------------|
| `{#HELP}`           | `$.help`                                               |
| `{#IFALIAS}`        | `$.labels.ifalias`                                     |
| `{#IFNAME}`         | `$.labels.device`                                      |
| `{#IFOPERSTATUS}`   | `$.labels.operstate`                                   |

| Filter macro        | Condition(And) | Regular expression                    |
|---------------------|----------------|---------------------------------------|
| `{#IFALIAS}`        | matches        | `{$NET.IF.IFALIAS.MATCHES}`           |
| `{#IFALIAS}`        | does not match | `{$NET.IF.IFALIAS.NOT_MATCHES}`       |
| `{#IFNAME}`         | matches        | `{$NET.IF.IFNAME.MATCHES}`            |
| `{#IFNAME}`         | does not match | `{$NET.IF.IFNAME.NOT_MATCHES}`        |
| `{#IFOPERSTATUS}`   | matches        | `{$NET.IF.IFOPERSTATUS.MATCHES}`      |
| `{#IFOPERSTATUS}`   | does not match | `{$NET.IF.IFOPERSTATUS.NOT_MATCHES}`  |

## New Rule (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Network interface discovery                            |
| Type                |   | Dependent item                                         |
| Key                 |   | `net.if.discovery[node_exporter]`                      |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_network_info' && @.metric.device=~'{$NET.IF.IFNAME.MATCHES}' && @.metric.ifalias=~'{$NET.IF.IFALIAS.MATCHES}' && @.metric.operstate=~'{$NET.IF.IFOPERSTATUS.MATCHES}')].metric` |
| JavaScript          | * | (See content below)                                    |

```javascript
return JSON.stringify(JSON.parse(value).map(
    function(metric){if(!("ifalias" in metric)) {metric.ifalias=""} return metric}
))
```

| LLD macro           | M | JSONPath                                               |
|---------------------|---|--------------------------------------------------------|
| `{#HELP}`           | * | (removed)                                              |
| `{#IFALIAS}`        | * | `$.ifalias`                                            |
| `{#IFNAME}`         | * | `$.device`                                             |
| `{#IFOPERSTATUS}`   | * | `$.operstate`                                          |

| Filter macro        | M | Condition(And) | Regular expression                    |
|---------------------|---|----------------|---------------------------------------|
| `{#IFALIAS}`        | * | matches        | (removed)                             |
| `{#IFALIAS}`        |   | does not match | `{$NET.IF.IFALIAS.NOT_MATCHES}`       |
| `{#IFNAME}`         | * | matches        | (removed)                             |
| `{#IFNAME}`         |   | does not match | `{$NET.IF.IFNAME.NOT_MATCHES}`        |
| `{#IFOPERSTATUS}`   | * | matches        | (removed)                             |
| `{#IFOPERSTATUS}`   |   | does not match | `{$NET.IF.IFOPERSTATUS.NOT_MATCHES}`  |
