Interface {#IFNAME}({#IFALIAS}): Interface type
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Interface {#IFNAME}({#IFALIAS}): Interface type        |
| Type                | Dependent item                                         |
| Key                 | `net.if.type[node_exporter,"{#IFNAME}"]`               |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (unsigned)`                                   |
| Units               |                                                        |
| Applications        | Interface {#IFNAME}({#IFALIAS})                        |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `node_network_protocol_type{device="{#IFNAME}"}`       |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Interface {#IFNAME}({#IFALIAS}): Interface type        |
| Type                |   | Dependent item                                         |
| Key                 |   | `net.if.type[node_exporter,"{#IFNAME}"]`               |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (unsigned)`                                   |
| Units               |   |                                                        |
| Applications        |   | Interface {#IFNAME}({#IFALIAS})                        |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_network_protocol_type' && @.metric.device=='{#IFNAME}')].value[1]` |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
