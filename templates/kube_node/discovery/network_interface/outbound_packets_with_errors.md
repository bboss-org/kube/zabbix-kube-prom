Interface {#IFNAME}({#IFALIAS}): Outbound packets with errors
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Interface {#IFNAME}({#IFALIAS}): Outbound packets with errors |
| Type                | Dependent item                                         |
| Key                 | `net.if.out.errors[node_exporter"{#IFNAME}"]`          |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               |                                                        |
| Applications        | Interface {#IFNAME}({#IFALIAS})                        |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `node_network_transmit_errs_total{device="{#IFNAME}"}` |
| Change per second   |                                                        |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Interface {#IFNAME}({#IFALIAS}): Outbound packets with errors |
| Type                |   | Dependent item                                         |
| Key                 |   | `net.if.out.errors[node_exporter"{#IFNAME}"]`          |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   |                                                        |
| Applications        |   | Interface {#IFNAME}({#IFALIAS})                        |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_network_transmit_errs_total' && @.metric.device=='{#IFNAME}')].value[1]` |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
| Change per second   |   |                                                        |
