Interface {#IFNAME}({#IFALIAS}): Bits received
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Interface {#IFNAME}({#IFALIAS}): Bits received         |
| Type                | Dependent item                                         |
| Key                 | `net.if.in[node_exporter,"{#IFNAME}"]`                 |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               | bps                                                    |
| Applications        | Interface {#IFNAME}({#IFALIAS})                        |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `node_network_receive_bytes_total{device="{#IFNAME}"}` |
| Change per second   |                                                        |
| Custom multiplier   | 8                                                      |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Interface {#IFNAME}({#IFALIAS}): Bits received         |
| Type                |   | Dependent item                                         |
| Key                 |   | `net.if.in[node_exporter,"{#IFNAME}"]`                 |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   | bps                                                    |
| Applications        |   | Interface {#IFNAME}({#IFALIAS})                        |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_network_receive_bytes_total' && @.metric.device=='{#IFNAME}')].value[1]` |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
| Change per second   |   |                                                        |
| Custom multiplier   |   | 8                                                      |
