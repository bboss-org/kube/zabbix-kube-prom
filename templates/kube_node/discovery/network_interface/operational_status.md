Interface {#IFNAME}({#IFALIAS}): Operational status
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Interface {#IFNAME}({#IFALIAS}): Operational status    |
| Type                | Dependent item                                         |
| Key                 | `net.if.status[node_exporter,"{#IFNAME}"]`             |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (unsigned)`                                   |
| Units               |                                                        |
| Applications        | Interface {#IFNAME}({#IFALIAS})                        |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `node_network_info{device="{#IFNAME}"}` `operstate`    |
| JavaScript          | (See content below)                                    |

```javascript
var newvalue;
switch(value) {
  case "up":
    newvalue = 1;
    break;
  case "down":
    newvalue = 2;
    break;
  case "testing":
    newvalue = 4;
    break;
  case "unknown":
    newvalue = 5;
    break;
  case "dormant":
    newvalue = 6;
    break;
  case "notPresent":
    newvalue = 7;
    break;
  default:
    newvalue = "Problem parsing interface operstate in JS";
}
return newvalue;
```

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Interface {#IFNAME}({#IFALIAS}): Operational status    |
| Type                |   | Dependent item                                         |
| Key                 |   | `net.if.status[node_exporter,"{#IFNAME}"]`             |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (unsigned)`                                   |
| Units               |   |                                                        |
| Applications        |   | Interface {#IFNAME}({#IFALIAS})                        |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_network_info' && @.metric.device=='{#IFNAME}')].metric.operstate` |
| JavaScript          | * | (See content below)                                    |

```javascript
var newvalue;
switch(JSON.parse(value)[0]) {
  case "up":
    newvalue = 1;
    break;
  case "down":
    newvalue = 2;
    break;
  case "testing":
    newvalue = 4;
    break;
  case "unknown":
    newvalue = 5;
    break;
  case "dormant":
    newvalue = 6;
    break;
  case "notPresent":
    newvalue = 7;
    break;
  default:
    newvalue = "Problem parsing interface operstate in JS";
}
return newvalue;
```
