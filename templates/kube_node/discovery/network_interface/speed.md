Interface {#IFNAME}({#IFALIAS}): Speed
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Interface {#IFNAME}({#IFALIAS}): Speed                 |
| Type                | Dependent item                                         |
| Key                 | `net.if.speed[node_exporter,"{#IFNAME}"]`              |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (unsigned)`                                   |
| Units               | bps                                                    |
| Applications        | Interface {#IFNAME}({#IFALIAS})                        |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `node_network_speed_bytes{device="{#IFNAME}"}`         |
|  Custom on fail     | Set Value to 0                                         |
| Custom multiplier   | 8                                                      |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Interface {#IFNAME}({#IFALIAS}): Speed                 |
| Type                |   | Dependent item                                         |
| Key                 |   | `net.if.speed[node_exporter,"{#IFNAME}"]`              |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (unsigned)`                                   |
| Units               |   | bps                                                    |
| Applications        |   | Interface {#IFNAME}({#IFALIAS})                        |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_network_speed_bytes' && @.metric.device=='{#IFNAME}')].value[1]` |
|  Custom on fail     | * | Set Value to ["0"]                                     |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
| Custom multiplier   |   | 8                                                      |
