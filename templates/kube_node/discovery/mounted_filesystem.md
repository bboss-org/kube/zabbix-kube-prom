Mounted filesystem discovery
--------------------------------------------------------------------------------

## Original Rule (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Mounted filesystem discovery                           |
| Type                | Dependent item                                         |
| Key                 | `vfs.fs.discovery[node_exporter]`                      |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus to JSON  | `{__name__=~"^node_filesystem_size(?:_bytes)?$", mountpoint=~".+"}` |

| LLD macro           | JSONPath                                               |
|---------------------|--------------------------------------------------------|
| `{#FSDEVICE}`       | `$.labels.device`                                      |
| `{#FSNAME}`         | `$.labels.mountpoint`                                  |
| `{#FSTYPE}`         | `$.labels.fstype`                                      |
| `{#HELP}`           | `$.help`                                               |

| Filter macro        | Condition(And) | Regular expression                    |
|---------------------|----------------|---------------------------------------|
| `{#FSDEVICE}`       | does not match | `{$VFS.FS.FSDEVICE.NOT_MATCHES}`      |
| `{#FSNAME}`         | matches        | `{$VFS.FS.FSNAME.MATCHES}`            |
| `{#FSNAME}`         | does not match | `{$VFS.FS.FSNAME.NOT_MATCHES}`        |
| `{#FSNAME}`         | matches        | `{$VFS.FS.FSDEVICE.MATCHES}`          |
| `{#FSTYPE}`         | matches        | `{$VFS.FS.FSTYPE.MATCHES}`            |
| `{#FSTYPE}`         | does not match | `{$VFS.FS.FSTYPE.NOT_MATCHES}`        |

## New Rule (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Mounted filesystem discovery                           |
| Type                |   | Dependent item                                         |
| Key                 |   | `vfs.fs.discovery[node_exporter]`                      |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_filesystem_size_bytes' && @.metric.device=~'{$VFS.FS.FSDEVICE.MATCHES}' && @.metric.fstype=~'{$VFS.FS.FSTYPE.MATCHES}' && @.metric.mountpoint=~'{$VFS.FS.FSNAME.MATCHES}')].metric` |

| LLD macro           | M | JSONPath                                               |
|---------------------|---|--------------------------------------------------------|
| `{#FSDEVICE}`       | * | `$.device`                                             |
| `{#FSNAME}`         | * | `$.mountpoint`                                         |
| `{#FSTYPE}`         | * | `$.fstype`                                             |
| `{#HELP}`           | * | (removed)                                              |

| Filter macro        | M | Condition(And) | Regular expression                    |
|---------------------|---|----------------|---------------------------------------|
| `{#FSDEVICE}`       | * | matches        | (removed)                             |
| `{#FSDEVICE}`       |   | does not match | `{$VFS.FS.FSDEVICE.NOT_MATCHES}`      |
| `{#FSNAME}`         | * | matches        | (removed)                             |
| `{#FSNAME}`         |   | does not match | `{$VFS.FS.FSNAME.NOT_MATCHES}`        |
| `{#FSTYPE}`         | * | matches        | (removed)                             |
| `{#FSTYPE}`         |   | does not match | `{$VFS.FS.FSTYPE.NOT_MATCHES}`        |
