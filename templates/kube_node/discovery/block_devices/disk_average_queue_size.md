{#DEVNAME}: Disk average queue size (avgqu-sz)
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | {#DEVNAME}: Disk average queue size (avgqu-sz)         |
| Type                | Dependent item                                         |
| Key                 | `vfs.dev.queue_size[node_exporter,"{#DEVNAME}"]`       |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               |                                                        |
| Applications        | Disk {#DEVNAME}                                        |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `node_disk_io_time_weighted_seconds_total{device="{#DEVNAME}"}` |
| Change per second   |                                                        |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | {#DEVNAME}: Disk average queue size (avgqu-sz)         |
| Type                |   | Dependent item                                         |
| Key                 |   | `vfs.dev.queue_size[node_exporter,"{#DEVNAME}"]`       |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   |                                                        |
| Applications        |   | Disk {#DEVNAME}                                        |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_disk_io_time_weighted_seconds_total' && @.metric.device=='{#DEVNAME}')].value[1]` |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
| Change per second   |   |                                                        |
