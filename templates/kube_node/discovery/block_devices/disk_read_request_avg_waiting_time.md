{#DEVNAME}: Disk read request avg waiting time (r_await)
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | {#DEVNAME}: Disk read request avg waiting time (r_await) |
| Type                | Calculated                                             |
| Key                 | `vfs.dev.read.await[node_exporter,"{#DEVNAME}"]`       |
| Formula             | (See content below)                                    |
| Type of information | `Numeric (float)`                                      |
| Units               | !ms                                                    |
| Applications        | Disk {#DEVNAME}                                        |

```javascript
(last("vfs.dev.read.time.rate[node_exporter,\"{#DEVNAME}\"]")/(last("vfs.dev.read.rate[node_exporter,\"{#DEVNAME}\"]")+(last("vfs.dev.read.rate[node_exporter,\"{#DEVNAME}\"]")=0)))*1000*(last("vfs.dev.read.rate[node_exporter,\"{#DEVNAME}\"]") > 0)
```
