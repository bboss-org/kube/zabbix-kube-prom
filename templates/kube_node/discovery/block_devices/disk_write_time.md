{#DEVNAME}: Disk write time (rate)
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | {#DEVNAME}: Disk write time (rate)                     |
| Type                | Dependent item                                         |
| Key                 | `vfs.dev.write.time.rate[node_exporter,"{#DEVNAME}"]`  |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               |                                                        |
| Applications        | Zabbix raw items                                       |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `node_disk_write_time_seconds_total{device="{#DEVNAME}"}` |
| Change per second   |                                                        |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | {#DEVNAME}: Disk write time (rate)                     |
| Type                |   | Dependent item                                         |
| Key                 |   | `vfs.dev.write.time.rate[node_exporter,"{#DEVNAME}"]`  |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   |                                                        |
| Applications        | * | Disk {#DEVNAME}                                        |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_disk_write_time_seconds_total' && @.metric.device=='{#DEVNAME}')].value[1]` |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
| Change per second   |   |                                                        |
