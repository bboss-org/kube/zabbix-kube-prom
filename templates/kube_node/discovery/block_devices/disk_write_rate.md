{#DEVNAME}: Disk write rate
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | {#DEVNAME}: Disk write rate                            |
| Type                | Dependent item                                         |
| Key                 | `vfs.dev.write.rate[node_exporter,"{#DEVNAME}"]`       |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               | !w/s                                                   |
| Applications        | Disk {#DEVNAME}                                        |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `node_disk_writes_completed_total{device="{#DEVNAME}"}` |
| Change per second   |                                                        |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | {#DEVNAME}: Disk write rate                            |
| Type                |   | Dependent item                                         |
| Key                 |   | `vfs.dev.write.rate[node_exporter,"{#DEVNAME}"]`       |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   | !w/s                                                   |
| Applications        |   | Disk {#DEVNAME}                                        |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_disk_writes_completed_total' && @.metric.device=='{#DEVNAME}')].value[1]` |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
| Change per second   |   |                                                        |
