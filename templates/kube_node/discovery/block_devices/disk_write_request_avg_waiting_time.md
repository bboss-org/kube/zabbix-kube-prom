{#DEVNAME}: Disk write request avg waiting time (w_await)
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | {#DEVNAME}: Disk write request avg waiting time (w_await) |
| Type                | Calculated                                             |
| Key                 | `vfs.dev.write.await[node_exporter,"{#DEVNAME}"]`      |
| Formula             | (See content below)                                    |
| Type of information | `Numeric (float)`                                      |
| Units               | !ms                                                    |
| Applications        | Disk {#DEVNAME}                                        |

```javascript
(last("vfs.dev.write.time.rate[node_exporter,\"{#DEVNAME}\"]")/(last("vfs.dev.write.rate[node_exporter,\"{#DEVNAME}\"]")+(last("vfs.dev.write.rate[node_exporter,\"{#DEVNAME}\"]")=0)))*1000*(last("vfs.dev.write.rate[node_exporter,\"{#DEVNAME}\"]") > 0)
```
