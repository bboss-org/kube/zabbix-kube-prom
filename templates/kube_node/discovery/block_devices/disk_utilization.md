{#DEVNAME}: Disk utilization
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | {#DEVNAME}: Disk utilization                           |
| Type                | Dependent item                                         |
| Key                 | `vfs.dev.util[node_exporter,"{#DEVNAME}"]`             |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               | %                                                      |
| Applications        | Disk {#DEVNAME}                                        |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `node_disk_io_time_seconds_total{device="{#DEVNAME}"}` |
| Change per second   |                                                        |
| Custom multiplier   | 100                                                    |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | {#DEVNAME}: Disk utilization                           |
| Type                |   | Dependent item                                         |
| Key                 |   | `vfs.dev.util[node_exporter,"{#DEVNAME}"]`             |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   | %                                                      |
| Applications        |   | Disk {#DEVNAME}                                        |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_disk_io_time_seconds_total' && @.metric.device=='{#DEVNAME}')].value[1]` |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
| Change per second   |   |                                                        |
| Custom multiplier   |   | 100                                                    |
