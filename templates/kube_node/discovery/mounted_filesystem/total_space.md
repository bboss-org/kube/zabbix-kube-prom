{#FSNAME}: Total space
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | {#FSNAME}: Total space                                 |
| Type                | Dependent item                                         |
| Key                 | `vfs.fs.total[node_exporter,"{#FSNAME}"]`              |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               | B                                                      |
| Applications        | Filesystem {#FSNAME}                                   |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus Pattern  | `{__name__=~"^node_filesystem_size(?:_bytes)?$", mountpoint="{#FSNAME}"}` |

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | {#FSNAME}: Total space                                 |
| Type                |   | Dependent item                                         |
| Key                 |   | `vfs.fs.total[node_exporter,"{#FSNAME}"]`              |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   | B                                                      |
| Applications        |   | Filesystem {#FSNAME}                                   |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_filesystem_size_bytes' && @.metric.device=='{#FSDEVICE}' && @.metric.fstype=='{#FSTYPE}' && @.metric.mountpoint=='{#FSNAME}')].value[1]` |
| JavaScript          | * | `return JSON.parse(value).map(Number)`                 |
