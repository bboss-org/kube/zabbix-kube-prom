{#FSNAME}: Space utilization
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | {#FSNAME}: Space utilization                           |
| Type                | Calculated                                             |
| Key                 | `vfs.fs.pused[node_exporter,"{#FSNAME}"]`              |
| Formula             | (See content below)                                    |
| Type of information | `Numeric (float)`                                      |
| Units               | %                                                      |
| Applications        | Filesystem {#FSNAME}                                   |

```javascript
(last("vfs.fs.used[node_exporter,\"{#FSNAME}\"]")/last("vfs.fs.total[node_exporter,\"{#FSNAME}\"]"))*100
```
