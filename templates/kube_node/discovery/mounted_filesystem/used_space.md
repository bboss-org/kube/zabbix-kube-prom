{#FSNAME}: Used space
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | {#FSNAME}: Used space                                  |
| Type                | Calculated                                             |
| Key                 | `vfs.fs.used[node_exporter,"{#FSNAME}"]`               |
| Formula             | (See content below)                                    |
| Type of information | `Numeric (float)`                                      |
| Units               | B                                                      |
| Applications        | Filesystem {#FSNAME}                                   |

```javascript
(last("vfs.fs.total[node_exporter,\"{#FSNAME}\"]")-last("vfs.fs.free[node_exporter,\"{#FSNAME}\"]"))
```
