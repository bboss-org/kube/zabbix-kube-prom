{#FSNAME}: Free inodes in %
--------------------------------------------------------------------------------

## Original Item (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | {#FSNAME}: Free inodes in %                            |
| Type                | Dependent item                                         |
| Key                 | `vfs.fs.inode.pfree[node_exporter,"{#FSNAME}"]`        |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |
| Type of information | `Numeric (float)`                                      |
| Units               | %                                                      |
| Applications        | Filesystem {#FSNAME}                                   |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus to JSON  | `{__name__=~"node_filesystem_files.*",mountpoint="{#FSNAME}"}` |
| JavaScript          | (See content below)                                    |

```javascript
//count vfs.fs.inode.pfree
var inode_free;
var inode_total;
JSON.parse(value).forEach(function(metric) {
  if (metric['name'] == 'node_filesystem_files'){
      inode_total = metric['value'];
  } else if (metric['name'] == 'node_filesystem_files_free'){
      inode_free = metric['value'];
  }
});
return (inode_free/inode_total)*100;
```

## New Item (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | {#FSNAME}: Free inodes in %                            |
| Type                |   | Dependent item                                         |
| Key                 |   | `vfs.fs.inode.pfree[node_exporter,"{#FSNAME}"]`        |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |
| Type of information |   | `Numeric (float)`                                      |
| Units               |   | %                                                      |
| Applications        |   | Filesystem {#FSNAME}                                   |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=~'node_filesystem_files.*' && @.metric.device=='{#FSDEVICE}' && @.metric.fstype=='{#FSTYPE}' && @.metric.mountpoint=='{#FSNAME}')]` |
| JavaScript          | * | (See content below)                                    |

```javascript
//count vfs.fs.inode.pfree
var inode_free;
var inode_total;
JSON.parse(value).forEach(function(value) {
  if (value.metric['__name__'] == 'node_filesystem_files'){
      inode_total = value.value[1];
  } else if (value.metric['__name__'] == 'node_filesystem_files_free'){
      inode_free = value.value[1];
  }
});
return (inode_free/inode_total)*100;
```
