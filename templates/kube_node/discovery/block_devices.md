Block devices discovery
--------------------------------------------------------------------------------

## Original Rule (Template OS Linux by Prom)
| Field               | Value                                                  |
|---------------------|--------------------------------------------------------|
| Name                | Block devices discovery                                |
| Type                | Dependent item                                         |
| Key                 | `vfs.dev.discovery[node_exporter]`                     |
| Master item         | `Template OS Linux by Prom: Get node_exporter metrics` |

| Preprocessing       | Parameters                                             |
|---------------------|--------------------------------------------------------|
| Prometheus to JSON  | `node_disk_io_now{device=~".+"}`                       |

| LLD macro           | JSONPath                                               |
|---------------------|--------------------------------------------------------|
| `{#DEVNAME}`        | `$.labels.device`                                      |
| `{#HELP}`           | `$.help`                                               |

| Filter macro        | Condition(And) | Regular expression                    |
|---------------------|----------------|---------------------------------------|
| `{#DEVNAME}`        | matches        | `{$VFS.DEV.DEVNAME.MATCHES}`          |
| `{#DEVNAME}`        | does not match | `{$VFS.DEV.DEVNAME.NOT_MATCHES}`      |

## New Rule (Template Kube Node by Prom API)
| Field               | M | Value                                                  |
|---------------------|---|--------------------------------------------------------|
| Name                |   | Block devices discovery                                |
| Type                |   | Dependent item                                         |
| Key                 |   | `vfs.dev.discovery[node_exporter]`                     |
| Master item         | * | `Template Kube Node by Prom API: Get node_exporter metrics` |

| Preprocessing       | M | Parameters                                             |
|---------------------|---|--------------------------------------------------------|
| JSONPath            | * | `$[?(@.metric['__name__']=='node_disk_io_now' && @.metric.device=~'{$VFS.DEV.DEVNAME.MATCHES}')].metric` |

| LLD macro           | M | JSONPath                                               |
|---------------------|---|--------------------------------------------------------|
| `{#DEVNAME}`        | * | `$.device`                                             |
| `{#HELP}`           | * | (removed)                                              |

| Filter macro        | M | Condition(And) | Regular expression                    |
|---------------------|---|----------------|---------------------------------------|
| `{#DEVNAME}`        | * | matches        | (removed)                             |
| `{#DEVNAME}`        |   | does not match | `{$VFS.DEV.DEVNAME.NOT_MATCHES}`      |
