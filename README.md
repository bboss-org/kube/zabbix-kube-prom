# Description

zabbix-kube-prom is a batch of Zabbix LLD templates for Zabbix server >= 4.4.

It is used for external Kubernetes monitoring by Zabbix via Prometheus API.

# Installation

1. Install [kube-prometheus-stack](https://github.com/prometheus-community/helm-charts/tree/main/charts/kube-prometheus-stack)
into the Kubernetes cluster.
2. Import global Zabbix Template ([zabbix-kube-prom.xml](zabbix-kube-prom.xml)) into your Zabbix server.
3. Create or [import](#cluster-host-samples) a host identifying your Kubernetes cluster where Prometheus is deployed.
4. Let LLD create discovered nodes as new "Zabbix hosts"
5. Let LLD create discovered pods as new "Virtual Zabbix hosts"

# Templates

The global export ([zabbix-kube-prom.xml](zabbix-kube-prom.xml)) contains following templates:

| Templates                                                                   | Description                                              |
|-----------------------------------------------------------------------------|----------------------------------------------------------|
| [Template Kube by Prom API](templates/template_kube_prom_api.xml)           | Creates a Zabbix host for each pod and node discovered.  |
| [Template Kube Node by Prom API](templates/template_kube_node_prom_api.xml) | Template applied to the created host (node).             |
| [Template Kube Pod by Prom API](templates/template_kube_pod_prom_api.xml)   | Template applied to the created host (pod).              |

# Zabbix Macros

| Host Macros                         | Description                                                                   |
|-------------------------------------|-------------------------------------------------------------------------------|
| `{$PROM.API.URL}`                   | Prometheus API URL (e.g. http://prometheus.k8scluster.nuci7.lan:8080/api/v1/) |
| `{$PROM.NODE.IP.MATCHES}`           | Regex used to filter Node discovery on Node IP address                        |
| `{$PROM.NODE.IP.NOT_MATCHES}`       | Regex used to filter Node discovery on Node IP address                        |
| `{$PROM.NODE.NAME.MATCHES}`         | Regex used to filter Node discovery on Node name                              |
| `{$PROM.NODE.NAME.NOT_MATCHES}`     | Regex used to filter Node discovery on Node name                              |
| `{$PROM.POD.NAME.MATCHES}`          | Regex used to filter Pod discovery on Pod name                                |
| `{$PROM.POD.NAME.NOT_MATCHES}`      | Regex used to filter Pod discovery on Pod name                                |
| `{$PROM.POD.NAMESPACE.MATCHES}`     | Regex used to filter Pod discovery on Namespace name                          |
| `{$PROM.POD.NAMESPACE.NOT_MATCHES}` | Regex used to filter Pod discovery on Namespace name                          |
| `{$PROM.POD.SERVICE.MATCHES}`       | Regex used to filter Pod discovery on Service name                            |
| `{$PROM.POD.SERVICE.NOT_MATCHES}`   | Regex used to filter Pod discovery on Service name                            |

Node discovery filters are cumulative.

Pod discovery filters are cumulative.

Original [Template OS Linux by Prom](https://github.com/zabbix/zabbix/tree/5.0.1/templates/os/linux_prom#macros-used) macros are also available
to propagate the value to discovered nodes.

# Cluster host samples

Several cluster host samples are provided in the `templates/kube/hosts/` directory.

When all templates ([zabbix-kube-prom.xml](zabbix-kube-prom.xml)) are available into the Zabbix Server, import the
desired host sample and modify it as you wish.

| Samples                                                                     | Description                             |
|-----------------------------------------------------------------------------|-----------------------------------------|
| [k8s-cluster](templates/kube/hosts/k8s-cluster.xml)                         | Creates a host for all existing pods    |
| [k8s-cluster_grafana](templates/kube/hosts/k8s-cluster_grafana.xml)         | Creates a host for the grafana pod      |
| [k8s-cluster_kube-system](templates/kube/hosts/k8s-cluster_kube-system.xml) | Creates a host for all kube-system pods |
| [k8s-cluster_nexus](templates/kube/hosts/k8s-cluster_nexus.xml)             | Creates a host for all nexus pods       |

# Credits

[Template Kube Node by Prom API](templates/template_kube_node_prom_api.xml) is the translation of the 
[Template OS Linux by Prom](https://github.com/zabbix/zabbix/tree/5.0.1/templates/os/linux_prom) from node exporter 
output `(Prometheus Pattern)` into Prometheus API data acquisition `(JSONPath)`.

Translated items are fully documented into `templates/kubenode/` directory.

# Licenses

| Template                  | License  |
|---------------------------|----------|
| Template OS Linux by Prom | *GNU General Public License v2.0 or later*<br>[Copyright (C) 2001-2021 Zabbix SIA](https://github.com/zabbix/zabbix/blob/master/README)
| Template Kube by Prom API<br>Template Kube Node by Prom API<br>Template Kube Pod by Prom API | *GNU General Public License v3.0*<br>[Copyright (C) 2021 Diagnostica Stago](https://www.stago.com/) |
